# steamer

Note: This script is intended to be deployed from debian-based machines only at this point (planned feature)

Features:
- Fully open source stack, using only trusted system tools and official sources/methods
- All configuration done over SSH only - no custom tooling or vendors to rely on
- CentOS 7.6 Server base image
- Definable PostgreSQL and confluence release version (default: postgresql-9.5/confluence-8.2.4)
- Terraform provisioning of Azure VM and associated resources
- Ansible configuration of VMs:
    - nginx reverse proxy (web tier)
    - java-1.8.0-openjdk and confluence prerequistes (web-tier)
    - JVM custom configuration parameter configuration (web-tier)
    - low privilege Tomcat user (web tier)
    - confluence 8.2.4 install (web-tier)
    - postgresql install and user creation
- Automated installation of required provisioning tools
- Supports customization of resource naming scheme <APPNAME>-<ENVIRONMENT>
- Security minded design choices


Prereqs:
- Valid Azure subscription
- subscription_id
- client_id
- client_secret
- tenant_id

To Deploy:
- Clone image: `git clone https://github.com/66gmc1000/steamer.git`
- Export creds as environmental variables: `
`

- Switch to the repo root directory: `cd steamer`
- Execute `./deploy.sh` with required parameters:

`./deploy.sh --app=confluence --environment=play --size=Standard_DS1_v2 --region=eastus --hostname=confluence --subscriptionid=${ARM_SUBSCRIPTION_ID} --clientid=${ARM_CLIENT_ID} --clientsecret=${ARM_CLIENT_SECRET} --tenantid=${ARM_TENANT_ID}`

- Visit http://IPADDRESS/ to configure your newly install confluence instance and continue the installation

To Destroy:
- Switch to the repo root directory: `cd pressure-cooker`
- Execute `./destroy.sh` with required parameters:

`./destroy.sh  --subscriptionid=${ARM_SUBSCRIPTION_ID} --clientid=${ARM_CLIENT_ID} --clientsecret=${ARM_CLIENT_SECRET} --tenantid=${ARM_TENANT_ID}`

Confluence Defaults (can be edited via ansible/roles/confluence/defaults/main.yml):

confluence_user: confluence
confluence_group: confluence
confluence_home: /var/atlassian/confluence
confluence_download_dir: /confluencedownload
confluence_installation_dir: /opt/atlassian/confluence
confluence_attachments_dir: "{{ confluence_home }}/data/attachments"
confluence_version: 8.2.4
confluence_jvm_support_recommended_args: "-Datlassian.plugins.enable.wait=300"
confluence_required_args: "-Djava.awt.headless=true -Datlassian.standalone=confluence -Dorg.apache.jasper.runtime.BodyContentImpl.LIMIT_BUFFER=true -Dmail.mime.decodeparameters=true -Dorg.dom4j.factory=com.atlassian.core.xml.InterningDocumentFactory"
confluence_jmx_enabled: false
confluence_jmx_port: 8090
confluence_disable_notifications: true
confluence_jvm_minimum_memory: "2048m"
confluence_jvm_maximum_memory: "4096m"
confluence_server_port: 8005
confluence_connector_port: 8080
confluence_proxy_name:
confluence_context_path:
confluence_database_configuration: false
confluence_service_name: confluence
confluence_service_state: started
confluence_service_enabled: true

PostgreSQL Defaults (can be edited via ansible/roles/postgresql/defaults/main.yml):

- POSTGRESQL_DATABASE: confluencedb
- POSTGRESQL_DATABASE:USER: confluenceuser
- POSTGRESQL_DATABASE:PASSWORD: < randomly generated >
 

ToDo:
- confluence/defaults/main.yml - hardcoded db host
- tighten host firewalls - 22, 5432, 80
- tighten postgresql source/dest range 5432
- randomize db name, user and password for multitenant and security

serverID: B076-1JRH-60WD-HQD6

AAABew0ODAoPeNp9kUtvgkAUhff8CpJu2sUQwIrVhKQtTCLGV8G2m26meNVpYCB3Blv/fUfQVOtjeQfuOd859yZhyhxUmenapuv0Wvc91zODcKZnp2ssEUCsirIEtIY8BSGBzrnihfDpeEbjaRwl1BhX+SfgZPEqAaVPHCMohGKpGrMcfM6EVaxBQJY9LnPGMystcuOLI7NO9qYVpismIWQK/C0AsTvEdYyd9WxTQq0ZTEYjGgfR03D/if6UHDcHew/EtfccdKRtL4AkgGvAKPSf7Y5HnEHcJ579HpL+S+g1lCUW8ypV1nYgsliob4ZgaVm+Bl9hBc1vl+s5U+K5JBpSKBBMpBfSXKE5aXLno3MNozChYzJ0Wt222/Y6hp7845crwoliqAD9BcskGBNcMsElqxNGQha54Cw1AoT66f/ZsgbiTTNtF9yjJkCHxRK53JUYgkyRl7X0QDOYyY7BvG1udPfRM+maZVXt1UBfusK5fg/ND/f+NJv5F9Z4DSkwLQIVAJCJ5Xa1u9lRMm/06Eb518MYOQ7mAhRX1H94pyHGCs8EUuZO1kxNSnpFng==X02ii