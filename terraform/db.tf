
####
# start deployment
####


# Create public IPs
resource "azurerm_public_ip" "confluencestackdbpublicip" {
    name                         = "${var.app_name}-${var.environment}-db-PublicIP"
    location                     = "${var.region}"
    resource_group_name          = "${azurerm_resource_group.confluencestackgroup.name}"
    allocation_method            = "Dynamic"

    tags = {
        environment = "${var.environment}"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "confluencestackdbnsg" {
    name                = "${var.app_name}-${var.environment}-db-NetworkSecurityGroup"
    location            = "${var.region}"
    resource_group_name = "${azurerm_resource_group.confluencestackgroup.name}"
    
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "SQL"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "5432"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "block"
        priority                   = 999
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "106.12.199.29"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "${var.environment}"
    }
}

# Create network interface
resource "azurerm_network_interface" "confluencestackdbnic" {
    name                      = "${var.app_name}-${var.environment}-db-NIC"
    location                  = "${var.region}"
    resource_group_name       = "${azurerm_resource_group.confluencestackgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.confluencestackdbnsg.id}"

    ip_configuration {
        name                          = "${var.app_name}-${var.environment}-db-NicConfiguration"
        subnet_id                     = "${azurerm_subnet.confluencestacksubnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.confluencestackdbpublicip.id}"
    }

    tags = {
        environment = "${var.environment}"
    }
}


# Create virtual machine
resource "azurerm_virtual_machine" "confluencestackdbvm" {
    name                  = "${var.app_name}-${var.environment}-db-VM"
    location              = "${var.region}"
    resource_group_name   = "${azurerm_resource_group.confluencestackgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.confluencestackdbnic.id}"]
    vm_size               = "${var.instance_type}"

    storage_os_disk {
        name              = "${var.app_name}-${var.environment}-db-OsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "OpenLogic"
        offer     = "CentOS"
        sku       = "7.6"
        version   = "latest"
    }

    os_profile {
        computer_name  = "${var.hostname}-db"
        admin_username = "azureuser"
        admin_password = "Password1234!"
    }

    os_profile_linux_config {
        disable_password_authentication = false
        ssh_keys {
            path     = "/home/azureuser/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags = {
        environment = "${var.environment}"
    }
}