# specify providers

provider "azurerm" {
    subscription_id = "${var.subscription_id}"
    client_id       = "${var.client_id}"
    client_secret   = "${var.client_secret}"
    tenant_id       = "${var.tenant_id}"
}

# define variables

variable "subscription_id" {
  default = "test"
}

variable "client_id" {
  default = "test"
}

variable "client_secret" {
  default = "test"
}

variable "tenant_id" {
  default = "test"
}

variable "instance_type" {
  type = string
  default = "Standard_DS1_v2"
}

variable "region" {
  type = string
  default = "eastus"
}

variable "environment" {
  type = string
  default = "play"
}

variable "app_name" {
  type = string
  default = "confluence"
}


variable "hostname" {
  type = string
  default = "confluence"
}

variable "ssh_key" {
  default = "test"
}

# Create a resource group if it doesn’t exist
resource "azurerm_resource_group" "confluencestackgroup" {
    name     = "${var.app_name}-${var.environment}-ResourceGroup"
    location = "${var.region}"

    tags = {
        environment = "${var.environment}"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "confluencestacknetwork" {
    name                = "${var.app_name}-${var.environment}-vNet"
    address_space       = ["10.0.0.0/16"]
    location            = "${var.region}"
    resource_group_name = "${azurerm_resource_group.confluencestackgroup.name}"

    tags = {
        environment = "${var.environment}"
    }
}

# Create subnet
resource "azurerm_subnet" "confluencestacksubnet" {
    name                 = "${var.app_name}-${var.environment}-Subnet"
    resource_group_name  = "${azurerm_resource_group.confluencestackgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.confluencestacknetwork.name}"
    address_prefix       = "10.0.1.0/24"
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.confluencestackgroup.name}"
    }
    
    byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "mystorageaccount" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.confluencestackgroup.name}"
    location                    = "${var.region}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "${var.environment}"
    }
}
